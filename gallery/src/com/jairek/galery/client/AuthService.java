package com.jairek.galery.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.jairek.galery.shared.Person;

@RemoteServiceRelativePath("authservice")
public interface AuthService extends RemoteService {
	String isValidUserPassword(String login, String password);
	
	boolean isTokenValid(String token);
	
	Person login(String login, String password);

}
