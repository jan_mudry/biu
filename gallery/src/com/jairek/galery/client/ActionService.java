package com.jairek.galery.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.jairek.galery.shared.*;

@RemoteServiceRelativePath("actionservice")
public interface ActionService extends RemoteService {

	
	Album addAlbum(String name, Person p);
	List<Album> getAlbumsForPerson(int user_id);
	List<Photo> getPhotosWithAlbum(int album_id, int user_id);
	Photo getPhoto(int album_id, int user_id, WhichPhoto wchich, int actual_id);
	
}