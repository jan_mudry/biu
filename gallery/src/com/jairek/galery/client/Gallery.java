package com.jairek.galery.client;

import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormPanel.SubmitCompleteEvent;
import com.google.gwt.user.client.ui.Hidden;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.Panel;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.jairek.galery.shared.*;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Gallery implements EntryPoint {
	
	private final AuthServiceAsync authService = GWT.create(AuthService.class);
	private final ActionServiceAsync actionService = GWT.create(ActionService.class);
	private Person user = null;
	private int actualPhotoId;
	private int lastPhotoId;
	private int firstPhotoId;
	private int actualAlbumId;
	Label validatiorError = new Label();
	Image image;
	Label descriptionImage;
	Button bPrev, bNext, bFirst, bLast, bRandom = null;
	
	
	public void onModuleLoad() {
		image = new Image();
		descriptionImage = new Label();
		descriptionImage.getElement().addClassName("description-image");
		validatiorError.getElement().setClassName("message-box");	
		validatiorError.getElement().addClassName("hide");
		image.getElement().setId("album-photo");
			
	    if (user != null) {
	    	setNickName(user); 	
	    }
	    setPage(registrationForm());
	}
	
	
	
	
	void setNickName(Person user) {
		RootPanel.get("nickname").clear();
    	RootPanel.get("nickname").add( new Label("Zalogowany jako: " + user.getName())); 
	}
	
	
	void setMenu() {
		HorizontalPanel panel = new HorizontalPanel();
		Button addAlbum = new Button("Add Album");
		Button showAlbums = new Button("Show Albums");
		Button addPhoto = new Button("Add Photo");
		
		addAlbum.addClickHandler(new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				setPage(addAlbumForm());				
			}
		});
		addPhoto.addClickHandler(new ClickHandler() {			
			@Override
			public void onClick(ClickEvent event) {
				setPage(addPhotoForm());				
			}
		});
		
		showAlbums.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				setPage(showAllAlbums());		
			}
		});
		
		panel.add(addAlbum);
		panel.add(showAlbums);
		panel.add(addPhoto);
		
		RootPanel.get("menu").clear();
    	RootPanel.get("menu").add(panel);
	}

	class ShowAlbumHandler implements ClickHandler {
		int album_id;
		boolean check = false;
		public ShowAlbumHandler () {
		}
		public ShowAlbumHandler (int id) {
			album_id = id;
			actionService.getPhoto(album_id, user.getId(), WhichPhoto.FIRST, -1, new AsyncCallback<Photo>() {
				public void onFailure(Throwable caught) {	
				}
				public void onSuccess(Photo result) {
					if (result != null) {
						check =  true;
					} else {
						check = false;
					}
				}		
			});
		}
		public void onClick(ClickEvent event) {
			actualAlbumId = album_id;
			if (!check) {
				Window.alert("This album is empty");
			} else {
				setPage(galeryPanel(album_id));
			}

		}
		
	}
	
	HorizontalPanel getNavPhotoButtons(int album_id) {
		HorizontalPanel hpanel = new HorizontalPanel();
		hpanel.getElement().addClassName("nav-buttons-container");
				
		bPrev = new Button("<");
		bPrev.addClickHandler(new ClickNavigationButtonHandler(WhichPhoto.PREV, actualAlbumId));
		bNext = new Button(">");	
		bNext.addClickHandler(new ClickNavigationButtonHandler(WhichPhoto.NEXT, actualAlbumId));
		bFirst = new Button("<||");	
		bFirst.addClickHandler(new ClickNavigationButtonHandler(WhichPhoto.FIRST, actualAlbumId));
		bLast = new Button("||>");	
		bLast.addClickHandler(new ClickNavigationButtonHandler(WhichPhoto.LAST, actualAlbumId));
		bRandom = new Button("RANDOM");	
		bRandom.addClickHandler(new ClickNavigationButtonHandler(WhichPhoto.RANDOM, actualAlbumId));
		hpanel.add(bFirst);
		hpanel.add(bPrev);
		hpanel.add(bRandom);
		hpanel.add(bNext);
		hpanel.add(bLast);
		
		return hpanel;
	}
	
	VerticalPanel galeryPanel(final int album_id) {
		VerticalPanel vpanel = new VerticalPanel();
		vpanel.add(image);
		vpanel.add(descriptionImage);
		vpanel.getElement().setId("album-image-container");
		
		vpanel.add(getNavPhotoButtons(album_id));
		
		actionService.getPhoto(album_id, user.getId(), WhichPhoto.FIRST, -1, new AsyncCallback<Photo>() {
			public void onFailure(Throwable caught) {	
			}
			public void onSuccess(final Photo result) {
				if (result != null) {
					actionService.getPhoto(album_id, user.getId(), WhichPhoto.LAST, -1, new AsyncCallback<Photo>() {
						public void onFailure(Throwable caught) {	
						}
						public void onSuccess(Photo result2) {
							if (result2 != null) {
								lastPhotoId = result2.getId();
								firstPhotoId = result.getId();
								setPhoto(result);	
							}
						}		
					});
								
				}
			}		
		});
			
		return vpanel;
	}

	class ClickNavigationButtonHandler implements ClickHandler {
		int albumId;
		WhichPhoto which;
		public ClickNavigationButtonHandler(){}
		public ClickNavigationButtonHandler(WhichPhoto which2, int album_id){
			albumId = album_id;
			which = which2;
		}
		public void onClick(ClickEvent event) {			
			actionService.getPhoto(albumId, user.getId(), which, actualPhotoId, new AsyncCallback<Photo>() {
				public void onSuccess(Photo result) {
					setPhoto(result);
				}
				public void onFailure(Throwable caught) {}
			});
		}		
	} 	
	
	Panel showAllAlbums () {
		final VerticalPanel panel = new VerticalPanel();
		panel.setStyleName("show-all-albums");
		actionService.getAlbumsForPerson(user.getId(), new AsyncCallback<List<Album>>() {
			public void onFailure(Throwable caught) {}
			@SuppressWarnings("deprecation")
			public void onSuccess(List<Album> result) {
				for(Album album : result) {
					Hyperlink a = new Hyperlink(album.getName(), ""+album.getId());
					a.addClickHandler(new ShowAlbumHandler(album.getId()));
					panel.add(a);
				}			   				
			}
		
		});
		return panel;
	}
		
	Panel addAlbumForm() {
		final FormPanel form = new FormPanel();
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);
			
		VerticalPanel holder = new VerticalPanel(); 
		clearMessage();
		holder.add(validatiorError);
		holder.add(new Label("name"));
		final TextBox albumName = new TextBox();
		albumName.setName("name");
		holder.add(albumName);
		form.add(holder);
		Button submitButton = new Button("Create");
		holder.add(submitButton);

		submitButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (albumName.getValue() != "") {
					actionService.addAlbum(albumName.getText(), user, new AsyncCallback<Album>() {

						@Override
						public void onFailure(Throwable caught) {
							
						}

						@Override
						public void onSuccess(Album result) {
							if (result != null) {
								setMessage("Succesful created album: " + result.getName(), false);
								albumName.getElement().setPropertyString("value", "");
							} else {
								setMessage("Already exist album", true);
							}
						}
					
					});		
				} else {
					setMessage("Name is too short", true);
				}
						
			}
		});
		
		return form;
	}
	
	Panel addPhotoForm() {
		final FormPanel form = new FormPanel();
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);
		form.setAction(GWT.getModuleBaseURL()+"file_upload");
		
		VerticalPanel holder = new VerticalPanel(); 
		clearMessage();
		holder.add(validatiorError);
		holder.add(new Label("Desription"));
		final TextBox photoDescription= new TextBox();
		photoDescription.setName("description");
		holder.add(photoDescription);
		
		holder.add(new Label("Album"));
		final ListBox albums = getListBoxAlbums(true);
		albums.setName("album");
		holder.add(albums);
		Hidden userName = new Hidden();
		
		if (user != null) {
			userName.setValue(user.getName());
		}
		userName.setName("userName");
			
		holder.add(new Label("File"));
	    final FileUpload upload = new FileUpload();
	    upload.setName("uploadFormElement");
	    holder.add(upload);
		
	
		final Button sendButton = new Button("Add");
	    sendButton.addClickHandler(new ClickHandler() {
	    	public void onClick(ClickEvent event) {
	    		
	    		if (albums.getSelectedIndex() == -1) {
	    			setMessage("You must select album", true);
	    		} else if ("".equalsIgnoreCase(upload.getFilename())) {
	    			setMessage("You must add photo", true);
	    		} else {
	    			clearMessage();
		    		form.submit();
	    		}

	    	}
	    });
	    
	    form.addSubmitCompleteHandler(new FormPanel.SubmitCompleteHandler() {
		     
	        @SuppressWarnings({ "unused", "deprecation" })
	        public void onSubmitComplete(SubmitCompleteEvent event) {
	    	// call upload method from service with the file's name
	        	String result = event.getResults();	
	        	String filepath = null; 
	        	String message = null;
	        try {
	        	JSONObject response = (JSONObject) JSONParser.parse(result);
	             filepath = ((JSONString) response.get("OK")).stringValue();
	             if (message != null || message != "" ) {
		        	 setMessage("Succesful created", false);
		        	 form.reset();
		         } else {
		        	 setMessage("Something wrong", true); 
		         }
	         } catch (Exception e) {
	        	 JSONObject response = (JSONObject) JSONParser.parse(result);
	        	 message = ((JSONString) response.get("message")).stringValue();
	        	 if (message != null || message != "" ) {
		        	 setMessage(message, true);
		         }
	         }    	        	
	        }
	    });
		
		holder.add(sendButton);
		holder.add(userName);
		form.add(holder);
		return form;
	}
	
	ListBox getListBoxAlbums(final boolean dropdown)
	{
		final ListBox widget = new ListBox();	
	   
	    actionService.getAlbumsForPerson(user.getId(), new AsyncCallback<List<Album>>() {

			@Override
			public void onFailure(Throwable caught) {
				
			}
			@Override
			public void onSuccess(List<Album> result) {
				for(Album a : result) {
					 widget.addItem(a.getName(), ""+a.getId());
				}
			    if(!dropdown) widget.setVisibleItemCount(3);				
			}
		
		});	
	    return widget;	  
	}
	
	Panel registrationForm () {
		final FormPanel form = new FormPanel();
		form.setEncoding(FormPanel.ENCODING_MULTIPART);
		form.setMethod(FormPanel.METHOD_POST);

		VerticalPanel holder = new VerticalPanel(); 

		holder.add(new Label("nick"));
		final TextBox userName = new TextBox();
		userName.setName("nick");
		holder.add(userName);

		holder.add(new Label("Password"));
		final PasswordTextBox passwd = new PasswordTextBox();
		passwd.setName("password");
		holder.add(passwd);

		Button submitButton = new Button("Login");
		submitButton.getElement().addClassName("submit-button");
		
		
		submitButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				authService.login(userName.getText(), passwd.getText(), new AsyncCallback<Person>() {
					public void onFailure(Throwable caught) {
						Window.alert("failure");	
					}
					public void onSuccess(Person result) {
						if (result != null) {
							user = result;
							setNickName(user); 	
							setMenu();
							setPage(null);
						} else {
							Window.alert("ACCESS DENIED");
						}
						;							
					}
				});				
			}
		});
		
		holder.add(submitButton);
		
		form.add(holder);

		return form;	
	}

	void clearMessage () {
		validatiorError.setText("");
		validatiorError.getElement().addClassName("hide");
	}
	void setMessage (String text, boolean error) {
		validatiorError.setText(text);
		validatiorError.getElement().removeClassName("hide");
		if (error) {
			validatiorError.getElement().removeClassName("succes-message");	
			validatiorError.getElement().addClassName("validation-error");
		} else {
			validatiorError.getElement().removeClassName("validation-error");	
			validatiorError.getElement().addClassName("succes-message");
		}
	}
	
	void setPhoto(Photo photo) {
		if (photo != null) {
			image.setUrl(photo.getPath());
			actualPhotoId = photo.getId();;
			descriptionImage.setText(photo.getDescription());
			if (photo.getId() == firstPhotoId) {
				bFirst.setEnabled(false);
				bPrev.setEnabled(false);
			} else {
				bFirst.setEnabled(true);
				bPrev.setEnabled(true);
			}
			if (photo.getId() == lastPhotoId) {
				bLast.setEnabled(false);
				bNext.setEnabled(false);
			} else {
				bLast.setEnabled(true);
				bNext.setEnabled(true);
			}
			if  (photo.getId() != lastPhotoId && photo.getId() != firstPhotoId) {
				bFirst.setEnabled(true);
				bLast.setEnabled(true);
				bNext.setEnabled(true);
				bPrev.setEnabled(true);
			}
		}
	}
	
	void setPage(Panel p ) {

	    RootPanel.get("container").clear();
	    if (p != null) {
	    	 RootPanel.get("container").add(p);	
	    }
	}
}
