package com.jairek.galery.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.jairek.galery.shared.Person;

public interface AuthServiceAsync {

	void isValidUserPassword(String login, String password,
			AsyncCallback<String> callback);

	void isTokenValid(String token, AsyncCallback<Boolean> callback);
	
	void login(String login, String password, AsyncCallback<Person> callback);
	
	
	
}
