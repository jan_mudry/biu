package com.jairek.galery.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.jairek.galery.shared.*;

public interface ActionServiceAsync {
	void addAlbum(String name, Person p, AsyncCallback<Album> callback);
	void getAlbumsForPerson(int user_id, AsyncCallback<List<Album>> callback);
	void getPhotosWithAlbum(int album_id, int user_id, AsyncCallback<List<Photo>> callback);
	void getPhoto(int album_id, int user_id, WhichPhoto wchich, int actual_id, AsyncCallback<Photo> callback);
}
