package com.jairek.galery.shared;

public enum WhichPhoto {
	FIRST, LAST, NEXT, PREV, RANDOM
}
