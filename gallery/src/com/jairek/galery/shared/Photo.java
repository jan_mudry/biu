package com.jairek.galery.shared;

import java.io.Serializable;

public class Photo implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String path;
	private String description;
	private int album_id;
	
	public int getAlbumId() {
		return album_id;
	}
	public void setAlbumId(int album_id) {
		this.album_id = album_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
