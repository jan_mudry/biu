package com.jairek.galery.server;

import java.util.List;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.jairek.galery.client.ActionService;
import com.jairek.galery.shared.Album;
import com.jairek.galery.shared.Person;
import com.jairek.galery.shared.Photo;
import com.jairek.galery.shared.WhichPhoto;

public class ActionServiceImpl  extends RemoteServiceServlet implements ActionService {
	private static final long serialVersionUID = 1L;
	private EntityManager em = new EntityManager();
	
	public ActionServiceImpl() {
		
	}
	
	public Album addAlbum(String name, Person p) {
		Album a;
		a = em.getAlbum(name, p.getId());
		if (a == null) {
			a = new Album();
			a.setName(name);
			if (em.addAlbum(a, p) > 0) {
				return em.getAlbum(name, p.getId());
			} else {
				return null;
			}
		} else {
			return null;	
		}		
	}
	
	public List<Album> getAlbumsForPerson(int user_id){
		return em.getAlbumsForPerson(user_id);
	}

	@Override
	public List<Photo> getPhotosWithAlbum(int album_id, int user_id) {
		return em.getPhotosWithAlbum(album_id, user_id);
	}

	@Override
	public Photo getPhoto(int album_id, int user_id, WhichPhoto wchich, int actual_id) {
		return em.getPhoto(album_id, user_id, wchich, actual_id);
	}
}
