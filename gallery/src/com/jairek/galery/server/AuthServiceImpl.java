package com.jairek.galery.server;

import java.util.Date;
import java.util.HashMap;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.jairek.galery.client.AuthService;
import com.jairek.galery.shared.Album;
import com.jairek.galery.shared.Person;

public class AuthServiceImpl  extends RemoteServiceServlet implements AuthService {
	
	private static final long serialVersionUID = -2065412172405526426L;
	private EntityManager em = new EntityManager();
	public AuthServiceImpl() {
		
	}
	
	class TokenData {
		public String login;
		public String pass;
		public Date expires;
		public TokenData(String l, String p) {
			login = l;
			pass = p;
			expires = new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 2); // 2h
		}
	}
	static HashMap<String,TokenData> tokens = new HashMap<String,TokenData>();
	

	public String register(String login, String password) {
		Person p = new Person();
		p.setName(login);
		p.setPassword(password);	
		em.addPerson(p);
		p = em.getPerson(login, password);
		if (em != null) {
			return p.getName(); 
		}
		return null;
	}

	
	public Person login(String login, String password) {
		Person p;
		p = em.getPersonByNick(login);
		if (p != null) {
			if (p.getPassword().equals(password)) {
				return p;
			} else {
				return null;
			}
		} else {
			p = new Person();
			p.setName(login);
			p.setPassword(password);	
			if (em.addPerson(p) > 0 ) {
				p = em.getPersonByNick(login);
				Album al = new Album();
				al.setName("default");
				al.setUserId(p.getId());
				em.addAlbum(al, p);
				return p;
			} else {
				return null;
			}	
		}
	}
	
	@Override
	public String isValidUserPassword(String login, String password) {
		if (login.compareTo(password) == 0) {
			String newtokenname = String.valueOf(Math.random()) + login.hashCode() + password.hashCode();
			tokens.put(newtokenname, new TokenData(login, password));
			return newtokenname;
		}
		return "";
	}

	@Override
	public boolean isTokenValid(String token) {
		if (tokens.get(token) != null) {
			TokenData td =tokens.get(token);
			if (td.expires.after(new Date(System.currentTimeMillis()))) {
				return true;
			} else {
				tokens.remove(token);
				return false;
			}
		}
		return false;
	}
	public static boolean isTokenValidStatic(String token) {
		if (tokens.get(token) != null) {
			TokenData td =tokens.get(token);
			if (td.expires.after(new Date(System.currentTimeMillis()))) {
				return true;
			} else {
				tokens.remove(token);
				return false;
			}
		}
		return false;
	}

}