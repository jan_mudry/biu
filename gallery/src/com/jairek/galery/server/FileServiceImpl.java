package com.jairek.galery.server;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.servlet.ServletRequestContext;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.jairek.galery.client.FileService;
import com.jairek.galery.shared.Photo;

@SuppressWarnings("serial")
public class FileServiceImpl extends RemoteServiceServlet implements FileService {
	
	private static final String UPLOAD_DIRECTORY = "uploads" + File.separator;
	
	@Override
	protected void service(final HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		EntityManager em = new EntityManager();
		boolean isMultiPart = ServletFileUpload.isMultipartContent(new ServletRequestContext(request));
		
		FileItem uploadedFileItem = null;
		String fileName;
		String description = null;
		int album_id = -1;
		String userName = null;
		
		if(isMultiPart) {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			try {
				@SuppressWarnings("unchecked")
				List<FileItem> items = upload.parseRequest(request);
				for (FileItem fi : items) {
					if (fi.getFieldName().equalsIgnoreCase("uploadFormElement")) {
							uploadedFileItem = fi;
				    } else if(fi.getFieldName().equalsIgnoreCase("description")) {
						description =  fi.getString();
				    } else if(fi.getFieldName().equalsIgnoreCase("album")) {
						album_id =  Integer.parseInt(fi.getString());
				    } else if(fi.getFieldName().equalsIgnoreCase("userName")) {
						userName =  fi.getString();
				    }
				}
				
				PrintWriter out = response.getWriter();
				response.setContentType("text/html");
				
				if (album_id == -1) {
					 response.setStatus(HttpServletResponse.SC_OK); 
		        	 out.printf("{ \"message\": \"You have to select album\" }");
		        	 return;
				} 
				
				if(uploadedFileItem == null) {
					super.service(request, response);
					return;
				} else if(uploadedFileItem.getFieldName().equalsIgnoreCase(
						"uploadFormElement")) {
					fileName = uploadedFileItem.getName(); 					
					String direcotry_path = UPLOAD_DIRECTORY + File.separator + userName + File.separator + album_id;
					File tempDir = new File(direcotry_path);
					if (!tempDir.exists()) {
						tempDir.mkdirs();
					}
					
					File uploadedFile = new File(direcotry_path + File.separator, fileName);
					
					
		             
					
			        if (uploadedFile.createNewFile()) {
			            	 uploadedFileItem.write(uploadedFile);     
			         		 Photo photo = new Photo();
			         		 photo.setName(uploadedFile.getName());
			         		 photo.setDescription(description);
			         		 photo.setPath(uploadedFile.getPath());
			         		 photo.setAlbumId(album_id);
			         		 int count = em.addPhoto(photo);
			                 response.setStatus(HttpServletResponse.SC_CREATED);   
			                 out.printf("{\"OK\": \"" + uploadedFile.getPath() + "\"}", count);      
			        } else {
			        	 response.setStatus(HttpServletResponse.SC_OK); 
			        	 out.printf("{ \"message\": \"The photo already exist in this album\" }");
			        }
			        response.flushBuffer(); 
			    } 
			
			} catch(FileUploadException e) {
				e.printStackTrace();
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			super.service(request, response);
			return;
		}
	}
	
}