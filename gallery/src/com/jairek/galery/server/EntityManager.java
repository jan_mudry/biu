package com.jairek.galery.server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.jairek.galery.shared.Album;
import com.jairek.galery.shared.Person;
import com.jairek.galery.shared.Photo;
import com.jairek.galery.shared.WhichPhoto;



public class EntityManager {

	Connection connection     = null;
    String url          = "jdbc:mysql://localhost:3306/";
    String db           = "gallery";
    String driver       = "com.mysql.jdbc.Driver";
    String user         = "root";
    String pass         = "admin";

	private String createTablePerson = "CREATE TABLE  `person` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY ,`nick` VARCHAR( 25 ) NOT NULL , `password` VARCHAR( 25 ) NOT NULL) ";
	private String createTableAlbum = "CREATE TABLE  `album` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY , `name` VARCHAR( 25 ) NOT NULL , `user_id` INT NOT NULL) ";
	private String createTablePhoto = "CREATE TABLE  `photo` (`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY , `name` VARCHAR( 255 ) NOT NULL , `path` VARCHAR( 255 ) NOT NULL , `description` VARCHAR( 255 ) NULL , `album_id` INT NOT NULL)";
	
	
	private Statement statement;
	
	private PreparedStatement getPersonStmt;
	private PreparedStatement addPersonStmt;
	private PreparedStatement getPersonByNickStmt;
	private PreparedStatement addAlbumStmt;
	private PreparedStatement getAlbumStmt;
	private PreparedStatement getAlbumsForPersonStmt;
	private PreparedStatement addPhotoStmt;
	private PreparedStatement getPhotosWithAlbumStmt;
	private PreparedStatement getLastPhotoWithAlbumStmt;
	private PreparedStatement getFirstPhotoWithAlbumStmt;
	private PreparedStatement getNextPhotoWithAlbumStmt;
	private PreparedStatement getPrevPhotoWithAlbumStmt;
	private PreparedStatement getRandomPhotoWithAlbumStmt;
	
	public EntityManager() {
		try {
			 Class.forName(driver).newInstance();
			connection = DriverManager.getConnection(url+db, user, pass);
			statement = connection.createStatement();
			ResultSet rs = connection.getMetaData().getTables(null, null, null,
					null);
			boolean tablePersonExists = false;
			boolean tablePhotoExists = false;
			boolean tableAlbumExists = false;
			while (rs.next()) {
				if ("Person".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tablePersonExists = true;
				} else if ("Album".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tableAlbumExists = true;
				} else if ("Photo".equalsIgnoreCase(rs.getString("TABLE_NAME"))) {
					tablePhotoExists = true;
				}
			}

			if (!tablePersonExists) statement.executeUpdate(createTablePerson);
			if (!tableAlbumExists) statement.executeUpdate(createTableAlbum);
			if (!tablePhotoExists) statement.executeUpdate(createTablePhoto);
			
			getPersonStmt = connection.prepareStatement("SELECT * FROM  `person` WHERE  `nick` =  ? AND  `password` =  ?");
			getPersonByNickStmt = connection.prepareStatement("SELECT * FROM  `person` WHERE  `nick` =  ?");
			addPersonStmt = connection.prepareStatement("INSERT INTO `person` (`nick` ,`password`) VALUES (?, ?)");
			addAlbumStmt = connection.prepareStatement("INSERT INTO `album` (`name`, `user_id`) VALUES (?, ?)");
			addPhotoStmt = connection.prepareStatement("INSERT INTO `photo` (`name` ,`path`, `description`,`album_id`) VALUES (?, ?, ?, ?)");
			getAlbumStmt =  connection.prepareStatement("SELECT * FROM  `album` WHERE  `name` =  ? AND `user_id` = ?");
			getAlbumsForPersonStmt = connection.prepareStatement("SELECT * from `album` where `user_id` = ?");
			getPhotosWithAlbumStmt = connection.prepareStatement("SELECT photo.id, photo.name, photo.path, photo.album_id, photo.description FROM  `photo` INNER JOIN  `album` ON photo.album_id = album.id WHERE photo.album_id = ? AND album.user_id = ? ORDER BY  `photo`.`id` ASC");
			getRandomPhotoWithAlbumStmt = connection.prepareStatement("SELECT photo.id, photo.name, photo.path, photo.album_id, photo.description FROM  `photo` INNER JOIN  `album` ON photo.album_id = album.id WHERE photo.album_id = ? AND album.user_id = ? AND photo.id != ? ORDER BY  `photo`.`id`  ASC");
			getFirstPhotoWithAlbumStmt = connection.prepareStatement("SELECT photo.id, photo.name, photo.path, photo.album_id, photo.description FROM  `photo` INNER JOIN  `album` ON photo.album_id = album.id WHERE photo.album_id = ? AND album.user_id = ? ORDER BY  `photo`.`id` ASC");
			getLastPhotoWithAlbumStmt = connection.prepareStatement("SELECT photo.id, photo.name, photo.path, photo.album_id, photo.description FROM  `photo` INNER JOIN  `album` ON photo.album_id = album.id WHERE photo.album_id = ? AND album.user_id = ? ORDER BY  `photo`.`id` DESC LIMIT 1");
			getPrevPhotoWithAlbumStmt =  connection.prepareStatement("SELECT photo.id, photo.name, photo.path, photo.album_id, photo.description FROM  `photo` INNER JOIN  `album` ON photo.album_id = album.id WHERE photo.album_id = ? AND album.user_id = ? AND photo.id < ? ORDER BY  `photo`.`id` DESC LIMIT 1");
			getNextPhotoWithAlbumStmt =  connection.prepareStatement("SELECT photo.id, photo.name, photo.path, photo.album_id, photo.description FROM  `photo` INNER JOIN  `album` ON photo.album_id = album.id WHERE photo.album_id = ? AND album.user_id = ? AND photo.id > ? ORDER BY  `photo`.`id` ASC LIMIT 1");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}
	
	public Person getPerson(String name, String password) {
		Person p = new Person();
		
		try {
			getPersonStmt.setString(1, name);
			getPersonStmt.setString(2, password);
			
			ResultSet rs = getPersonStmt.executeQuery();
			if (rs.next() ) {
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("nick"));
				p.setPassword(rs.getString("password"));	
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return p;
	}
	
	public Person getPersonByNick(String nick) {
		Person p = new Person();
		
		try {
			getPersonByNickStmt.setString(1, nick);
			
			ResultSet rs = getPersonByNickStmt.executeQuery();
			if (rs.next() ) {
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("nick"));
				p.setPassword(rs.getString("password"));	
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return p;
	}
	
	public int addPerson(Person person) {
		int count = 0;
		try {
			addPersonStmt.setString(1, person.getName());
			addPersonStmt.setString(2, person.getPassword());

			count = addPersonStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public int addAlbum(Album album, Person p) {
		int count = 0;
		try {
			addAlbumStmt.setString(1, album.getName());
			addAlbumStmt.setInt(2, p.getId());

			count = addAlbumStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public Album getAlbum(String name, int user_id) {
		Album a = new Album();
		try {
			getAlbumStmt.setString(1, name);
			getAlbumStmt.setInt(2, user_id);
			
			ResultSet rs = getAlbumStmt.executeQuery();
			if (rs.next() ) {
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setUserId(rs.getInt("user_id"));	
			} else {
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return a;
	}
	
	public List<Album> getAlbumsForPerson(int user_id) {
		List<Album> albums = new ArrayList<Album>();
		try {
			getAlbumsForPersonStmt.setInt(1, user_id);
			ResultSet rs = getAlbumsForPersonStmt.executeQuery();

			while (rs.next()) {
				Album a = new Album();
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setUserId(rs.getInt("user_id"));
				albums.add(a);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return albums;
	}
	
	public int addPhoto(Photo p) {
		int count = 0;
		try {
			//(`name` ,`path`, `description`,`album_id`)
			addPhotoStmt.setString(1, p.getName());
			addPhotoStmt.setString(2, p.getPath());
			addPhotoStmt.setString(3, p.getDescription());
			addPhotoStmt.setInt(4, p.getAlbumId());

			count = addPhotoStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}
	
	public List<Photo> getPhotosWithAlbum(int album_id, int user_id) {
		List<Photo> photos = new ArrayList<Photo>();
		try {
			getPhotosWithAlbumStmt.setInt(2, user_id);
			getPhotosWithAlbumStmt.setInt(1, album_id);
			ResultSet rs = getPhotosWithAlbumStmt.executeQuery();

			while (rs.next()) {
				Photo a = new Photo();
				a.setId(rs.getInt("id"));
				a.setName(rs.getString("name"));
				a.setAlbumId(rs.getInt("album_id"));
				a.setPath(rs.getString("path"));
				a.setDescription(rs.getString("description"));
				photos.add(a);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return photos;
	}
	
	public Photo getPhoto(int album_id, int user_id, WhichPhoto wchich, int actual_id) {
		PreparedStatement statement = null;
		Photo photo = new Photo();
		try {
			switch (wchich) {
			case FIRST:
				statement = getFirstPhotoWithAlbumStmt;
				break;
			case LAST:
				statement = getLastPhotoWithAlbumStmt;
				break;
			case NEXT:
				statement = getNextPhotoWithAlbumStmt;
				statement.setInt(3, actual_id);
				break;
			case PREV:
				statement = getPrevPhotoWithAlbumStmt;
				statement.setInt(3, actual_id);
				break;
			case RANDOM:
				statement = getRandomPhotoWithAlbumStmt;
				statement.setInt(3, actual_id);
				break;
			}
			
			statement.setInt(2, user_id);
			statement.setInt(1, album_id);
			ResultSet rs = statement.executeQuery();
			int rowcount = 0;
			if (wchich == WhichPhoto.RANDOM) {
				if (rs.last()) {
					  rowcount = rs.getRow();
					  rs.beforeFirst();
					  int randomNum = 0 + (int)(Math.random()*rowcount); 
					  int i = 0;
					  boolean check = false;
					  while (rs.next()) {
						  if (i == randomNum) {
							  	check = true;
								photo.setId(rs.getInt("id"));
								photo.setName(rs.getString("name"));
								photo.setAlbumId(rs.getInt("album_id"));
								photo.setPath(rs.getString("path"));
								photo.setDescription(rs.getString("description"));  
						  }
						  i++;
						}
					  if (!check) { return null;}
				}	
			} else {
				if (rs.next()) {
					photo.setId(rs.getInt("id"));
					photo.setName(rs.getString("name"));
					photo.setAlbumId(rs.getInt("album_id"));
					photo.setPath(rs.getString("path"));
					photo.setDescription(rs.getString("description"));
				} else {
					return null;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return photo;

	}
	
}
