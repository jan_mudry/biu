package com.jairek.vadingallery.views;

import com.jairek.vadingallery.MyVaadinApplication;
import com.jairek.vadingallery.domain.Person;
import com.vaadin.ui.Label;
import com.vaadin.ui.Window;

public class AddPhoto extends Window {
	
	Person user = null; 
	public AddPhoto(){
		super("Add photo");
        setName ("add-photo");
        initUI();
	}
	
	 private void initUI () {
		 addComponent(MyVaadinApplication.getCurrent().getMenu()); 
		 addComponent(new Label("Add photo page!"));
	 }
}
