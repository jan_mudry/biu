
package com.jairek.vadingallery;

import org.vaadin.appfoundation.view.ViewHandler;
import com.jairek.vadingallery.domain.*;
import com.jairek.vadingallery.service.EntityManager;
import com.jairek.vadingallery.views.*;
import com.vaadin.Application;
import com.vaadin.service.ApplicationContext.TransactionListener;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

/**
 * The Application's "main" class
 */
@SuppressWarnings("serial")
public class MyVaadinApplication extends Application implements TransactionListener {

	private static ThreadLocal<MyVaadinApplication> currentApplication = new ThreadLocal<MyVaadinApplication>(); 	
	
    Person user = null;    
    Window mainWindow;
    
    public void setUser(Person user) {
    	this.user = user;    	
    }
    
    public Person getUser() {
    	return this.user;
    }
    
    private EntityManager em = new EntityManager();

    @Override
    public void init()
    {
    	mainWindow =  new LoginWindow();
    	setCurrent(this);
    	setTheme("style");
    	ViewHandler.initialize(this);
    	ViewHandler.getUriFragmentUtil();
    	
    	setMainWindow(mainWindow);
    	 if (getContext() != null) {
             getContext().addTransactionListener(this);
         }	

    }
       
    public VerticalLayout getMenu() {
		 VerticalLayout vertical = new VerticalLayout ();
		 if (getCurrent().getUser() != null){
			 vertical.addComponent(new Label ("Sign in " + getCurrent().getUser().getName()));
		 }	 
		 HorizontalLayout horizontal = new HorizontalLayout(); 
		 Button addAlbum = new Button("Add album");
		 addAlbum.addListener(new Button.ClickListener() {			
			public void buttonClick(ClickEvent event) {		
				ViewHandler.activateView(AddAlbumView.class);  											
			}
		 });
		 Button showAlbums = new Button("Show albums");
		 showAlbums.addListener(new Button.ClickListener() {			
				public void buttonClick(ClickEvent event) {	
					ViewHandler.deactivateView(ShowAlbumsView.class); 
					ViewHandler.activateView(ShowAlbumsView.class);  										
				}
			 });
		 Button addPhoto = new Button("Add Photo");
		 addPhoto.addListener(new Button.ClickListener() {			
				public void buttonClick(ClickEvent event) {		
					ViewHandler.activateView(AddPhotoView.class);  								
				}
			 });
		 horizontal.addComponent(addAlbum);
		 horizontal.addComponent(showAlbums);
		 horizontal.addComponent(addPhoto);
		 horizontal.addStyleName("center");
		 horizontal.setMargin(true,true,true,true);
		 
		 vertical.addComponent(horizontal);
		 vertical.setMargin(true,true,true,true);
		 vertical.addStyleName("menu-container");
		 vertical.setWidth("100%");
		 return vertical; 	
    }
    
    public static MyVaadinApplication getCurrent() {
        return currentApplication.get();
    }
    
    public static void setCurrent(MyVaadinApplication application) {
        currentApplication.set(application);
    }

    /**
     * Remove the current application instance
     */
    public static void removeCurrent() {
        currentApplication.remove();
    }
    

	public void transactionStart(Application application, Object transactionData) {
		 if ( application == MyVaadinApplication.this )
	        {
	            currentApplication.set ( this );
	        }
		
	}

	public void transactionEnd(Application application, Object transactionData) {
	      if ( application == MyVaadinApplication.this )
	        {
	            currentApplication.set ( null );
	            currentApplication.remove ();
	        }
		
	}
	

    
}
