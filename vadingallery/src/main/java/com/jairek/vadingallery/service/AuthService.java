package com.jairek.vadingallery.service;

import java.util.Date;
import java.util.HashMap;

import com.jairek.vadingallery.domain.*;


public class AuthService {
	
	private static final long serialVersionUID = -2065412172405526426L;
	private EntityManager em = new EntityManager();
	public AuthService() {
		
	}
	
	class TokenData {
		public String login;
		public String pass;
		public Date expires;
		public TokenData(String l, String p) {
			login = l;
			pass = p;
			expires = new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 2); // 2h
		}
	}
	static HashMap<String,TokenData> tokens = new HashMap<String,TokenData>();
	

	public String register(String login, String password) {
		Person p = new Person();
		p.setName(login);
		p.setPassword(password);	
		em.addPerson(p);
		p = em.getPerson(login, password);
		if (em != null) {
			return p.getName(); 
		}
		return null;
	}

	
	public Person login(String login, String password) throws Exception {
		Person p;
		p = em.getPersonByNick(login);
		if (p != null) {
			if (p.getPassword().equals(password)) {
				return p;
			} else {
				throw new Exception("Login failed!");
			}
		} else {
			p = new Person();
			p.setName(login);
			p.setPassword(password);	
			if (em.addPerson(p) > 0 ) {
				p = em.getPersonByNick(login);
				Album al = new Album();
				al.setName("default");
				al.setUserId(p.getId());
				em.addAlbum(al, p);
				return p;
			} else {
				throw new Exception("Register failed!");
			}	
		}
	}

}