package com.jairek.vadingallery.views;

import com.jairek.vadingallery.MyVaadinApplication;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class MainWindow extends Window{
	private static final long serialVersionUID = 1L;

	public MainWindow (){
	        super("Gallery");
	        setName ("main");
	        initUI();
	 }
	 
	 private void initUI () {
		 VerticalLayout vertical = new VerticalLayout ();
		 vertical.addComponent(MyVaadinApplication.getCurrent().getMenu());
		 vertical.addComponent(new MainView());
		 vertical.addStyleName("centermain");
		 vertical.setMargin(true,true,true,true);
		 addComponent(vertical);
	 }

}
