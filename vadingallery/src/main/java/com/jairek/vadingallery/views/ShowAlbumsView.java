package com.jairek.vadingallery.views;

import java.util.List;

import org.vaadin.appfoundation.view.AbstractView;
import org.vaadin.appfoundation.view.ViewHandler;

import com.jairek.vadingallery.MyVaadinApplication;
import com.jairek.vadingallery.domain.Album;
import com.jairek.vadingallery.domain.Photo;
import com.jairek.vadingallery.service.EntityManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class ShowAlbumsView  extends AbstractView<VerticalLayout> implements ClickListener {
	private static final long serialVersionUID = 1L;

	private EntityManager em = new EntityManager();
	
	
	@SuppressWarnings("serial")
	public ShowAlbumsView() {
		super(new VerticalLayout());
		
	}

	public void activated(Object... params) {
		Table table = new Table();
		table.addContainerProperty("Abum name", Button.class,  null);
		List<Album> albums = em.getAlbumsForPerson(MyVaadinApplication.getCurrent().getUser().getId());
		int i =0;
		for(final Album a : albums) {
			i++;
			Button b = new Button(a.getName());
			
			List<Photo> photos = em.getPhotosWithAlbum(a.getId(), MyVaadinApplication.getCurrent().getUser().getId());
			System.out.println("a.getId(): "+ a.getId() + " count: " + photos.size() + "empty: " + photos.isEmpty());
			b.setStyleName("link");
			if (!photos.isEmpty()) {
				b.addListener(new Button.ClickListener() {
					public void buttonClick(Button.ClickEvent event) {
						ViewHandler.deactivateView(ShowAlbumView.class);
						ViewHandler.activateView(ShowAlbumView.class, a.getId()); 
					}
				});	
			} else {
				System.out.println("b.setEnabled(false);");
				b.setEnabled(false);
			}

			table.addItem(new Object[] {
				   b}, new Integer(i));
		}
		addComponentInCenter(table);
	}

	public void deactivated(Object... params) {
		getContent().removeAllComponents();
	}

	public void buttonClick(ClickEvent event) {
	}
	
	public void addComponentInCenter(Component component) {
		getContent().addComponent(component);
		getContent().setComponentAlignment(component, Alignment.MIDDLE_CENTER);
	}
	
}
