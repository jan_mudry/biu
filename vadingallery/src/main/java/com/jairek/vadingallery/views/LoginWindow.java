package com.jairek.vadingallery.views;

import com.jairek.vadingallery.MyVaadinApplication;
import com.jairek.vadingallery.domain.Person;
import com.jairek.vadingallery.service.AuthService;
import com.vaadin.terminal.ExternalResource;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;

public class LoginWindow extends Window {
	private static final long serialVersionUID = 1L;
	private Button btnLogin = new Button("Login");
    private TextField login = new TextField ( "Username");
    private TextField password = new TextField ( "Password");
    private AuthService authService = new AuthService();
    
    
    public LoginWindow ()
    {
        super("Authentication Required !");
        setName ("login");
        initUI();
    }

    @SuppressWarnings("serial")
	private void initUI ()
    {
        password.setSecret ( true );
        addComponent ( new Label ("Please login in order to use the application") );
        addComponent ( new Label () );
        addComponent ( login );
        addComponent ( password );
        addComponent ( btnLogin );
        
        btnLogin.addListener ( new Button.ClickListener()
        {
            public void buttonClick ( Button.ClickEvent event )
            {
                try
                {
                	Person user = authService.login((String)login.getValue (), (String)password.getValue ());
                	if (user != null) {
                    	MyVaadinApplication.getCurrent().setUser(user);
                    	MyVaadinApplication.getCurrent().setMainWindow ( new MainWindow());
                    	open ( new ExternalResource (MyVaadinApplication.getCurrent().getURL ()));	
                	} else {
                		showNotification("Login incorect!");
                	}

                }
                catch ( Exception e )
                {
                    showNotification ( "Error" + e.getMessage(), Notification.TYPE_ERROR_MESSAGE );
                }
            }
        });
    }
}
