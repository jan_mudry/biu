package com.jairek.vadingallery.views;

import org.vaadin.appfoundation.view.AbstractView;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickListener;

public class WelcomeView extends AbstractView<VerticalLayout> implements ClickListener {
	private static final long serialVersionUID = 8401760557369059696L;
	
	public WelcomeView() {
		super(new VerticalLayout());
		Label label = new Label ("Welcome");
		getContent().addComponent (label);
		getContent().setComponentAlignment(label, Alignment.MIDDLE_CENTER);
		// TODO Auto-generated constructor stub
	}

	public void activated(Object... params) {
		// TODO Auto-generated method stub
		
	}

	public void deactivated(Object... params) {
		// TODO Auto-generated method stub
		
	}

	public void buttonClick(ClickEvent event) {
		// TODO Auto-generated method stub
		
	}

}
