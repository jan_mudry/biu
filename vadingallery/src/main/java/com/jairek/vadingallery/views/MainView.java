package com.jairek.vadingallery.views;

import org.vaadin.appfoundation.view.View;
import org.vaadin.appfoundation.view.ViewContainer;
import org.vaadin.appfoundation.view.ViewHandler;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

public class MainView extends CustomComponent implements ViewContainer {
	private static final long serialVersionUID = 1L;
	
	private VerticalLayout layout = new VerticalLayout();
	  private View currentView;

	  @SuppressWarnings("deprecation")
	public MainView() {
	    setCompositionRoot(layout);
	    layout.setWidth(500);
	    layout.setMargin(true);
	    layout.setStyleName("main-container");
	    ViewHandler.addView(LoginView.class, this);
	    ViewHandler.addUri("login", LoginView.class);
	    ViewHandler.addView(WelcomeView.class, this);
	    ViewHandler.addUri("welcome", WelcomeView.class);
	    ViewHandler.addView(AddPhotoView.class, this);
	    ViewHandler.addUri("add-photo", AddPhotoView.class);
	    ViewHandler.addView(AddAlbumView.class, this);
	    ViewHandler.addUri("add-album", AddAlbumView.class);
	    ViewHandler.addView(ShowAlbumsView.class, this);
	    ViewHandler.addUri("show-albums", ShowAlbumsView.class);
	    ViewHandler.addView(ShowAlbumView.class, this);
	    ViewHandler.addUri("show-album", ShowAlbumView.class);
	    // Set the default view
	    currentView = ViewHandler.getViewItem(WelcomeView.class).getView();
	    layout.addComponent(ViewHandler.getUriFragmentUtil());
	    layout.addComponent((Component) currentView);

	  }

	  public void activate(View view) {
	    if(!(view instanceof Component)) {
	       throw new IllegalArgumentException("View must be a component");
	    }
	    if (currentView == null) {
            layout.addComponent((Component) view);
        } else {
            layout.replaceComponent((Component) currentView, (Component) view);
        }
        currentView = view;
	  }



	public void deactivate(View view) {
		// TODO Auto-generated method stub			
	}
	}
