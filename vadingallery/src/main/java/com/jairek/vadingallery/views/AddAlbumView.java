package com.jairek.vadingallery.views;
import org.vaadin.appfoundation.view.AbstractView;
import com.jairek.vadingallery.MyVaadinApplication;
import com.jairek.vadingallery.domain.Album;
import com.jairek.vadingallery.service.EntityManager;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.Notification;

public class AddAlbumView  extends AbstractView<VerticalLayout> implements ClickListener {
	private static final long serialVersionUID = 1L;
	
	private EntityManager em = new EntityManager();
	private TextField albumName = new TextField ( "Name");
	private Button addButton = new Button("Add");
	
	public AddAlbumView() {
		super(new VerticalLayout());
		getContent().setMargin(true);
		addComponentInCenter ( albumName );
		addButton.addListener ( new Button.ClickListener() {
			private static final long serialVersionUID = 1L;

			public void buttonClick ( Button.ClickEvent event )
            {
            	Album album = new Album();
            	String albumNameString = (String)albumName.getValue();
            	if (albumNameString.equalsIgnoreCase("")) {
            		MyVaadinApplication.getCurrent().getMainWindow().showNotification ("This field is required!", Notification.TYPE_ERROR_MESSAGE);	
            	} else {
            		album.setName(albumNameString);
                	try {
    					if ( em.addAlbum(album, MyVaadinApplication.getCurrent().getUser()) > 0 ){
    						MyVaadinApplication.getCurrent().getMainWindow().showNotification("Succesful created album!", Notification.TYPE_HUMANIZED_MESSAGE);
    						albumName.setValue("");
    					} else {
    						MyVaadinApplication.getCurrent().getMainWindow().showNotification ("Something wrong", Notification.TYPE_ERROR_MESSAGE);
    					}
    				} catch (Exception e) {
    					MyVaadinApplication.getCurrent().getMainWindow().showNotification (e.getMessage(), Notification.TYPE_ERROR_MESSAGE);
    				}
            	}
            }
        });
		addComponentInCenter(addButton);
	}

	public void activated(Object... params) {
	}

	public void deactivated(Object... params) {
	}

	public void buttonClick(ClickEvent event) {
	}
	
	public void addComponentInCenter(Component component) {
		getContent().addComponent(component);
		getContent().setComponentAlignment(component, Alignment.MIDDLE_CENTER);
	}

}
