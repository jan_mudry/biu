package com.jairek.vadingallery.views;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.UUID;

import org.vaadin.appfoundation.view.AbstractView;
import com.jairek.vadingallery.MyVaadinApplication;
import com.jairek.vadingallery.domain.Album;
import com.jairek.vadingallery.domain.Photo;
import com.jairek.vadingallery.service.EntityManager;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Select;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class AddPhotoView extends AbstractView<VerticalLayout> implements ClickListener {
	private static final long serialVersionUID = 1L;
	
	private static final String UPLOAD_DIRECTORY = "uploads" + File.separator;
	EntityManager em = new EntityManager();
	final Upload upload;
	Select albumSelect ;
	TextField description = new TextField ( "Description");
	BeanItem<Photo> photoBeanItem;
	String path;
	
	@SuppressWarnings("serial")
	public AddPhotoView() {
		super(new VerticalLayout());
		upload = new Upload("", new Receiver() {			
			private static final long serialVersionUID = 1L;

			public OutputStream receiveUpload(String filename,
                    String MIMEType) {
				String direcotry_path = UPLOAD_DIRECTORY + File.separator + MyVaadinApplication.getCurrent().getUser().getName() + File.separator + ((Album) albumSelect.getValue()).getId();
				File tempDir = new File(direcotry_path);
				if (!tempDir.exists()) {
					tempDir.mkdirs();
				}
				FileOutputStream fos = null; // Output stream to write to
				File file = new File(direcotry_path + File.separator, ""+UUID.randomUUID()+".jpg");
				try {
					path = file.getPath();
					fos = new FileOutputStream(file);
				} catch (final java.io.FileNotFoundException e) {
					e.printStackTrace();
					return null;
				} 
				return fos; // Return the output stream to write to
			}
		});
		
		upload.addListener(new StartedListener() {
			private static final long serialVersionUID = 1L;
			public void uploadStarted(StartedEvent event) {
		    }
		});
		
		upload.addListener(new FailedListener() {
			public void uploadFailed(FailedEvent event) {
				MyVaadinApplication.getCurrent().getMainWindow().showNotification("Failed upload photo!", Notification.TYPE_ERROR_MESSAGE );	
			}
		});
		
		upload.addListener(new SucceededListener() {
			private static final long serialVersionUID = 1L;

			public void uploadSucceeded(SucceededEvent event) {
				em.addPhoto(new Photo(event.getFilename(),path, ""+description.getValue(),(Album) albumSelect.getValue() ));
				MyVaadinApplication.getCurrent().getMainWindow().showNotification("Succesfull upload photo!");
				resetForm();
			}
		});
		
		albumSelect = getSelectAlbums();
		getContent().addComponent(albumSelect);
		getContent().addComponent(description);
		getContent().addComponent(upload);
	}
	
	public void resetForm() {
		description.setValue("");
		albumSelect.setValue(albumSelect.getItemIds().iterator().next());
	}
	
	public Select getSelectAlbums() {
	    BeanItemContainer<Album> container = new BeanItemContainer<Album>(em.getAlbumsForPerson(MyVaadinApplication.getCurrent().getUser().getId()));

	    final Select select = new Select("Album", container);
	    select.setItemCaptionMode(Select.ITEM_CAPTION_MODE_PROPERTY);
	    select.setItemCaptionPropertyId("name");
	    select.setNullSelectionAllowed(false);
	    select.setValue(select.getItemIds().iterator().next());
	    return select;
	}

	public void activated(Object... params) {}
	public void deactivated(Object... params) {}
	public void buttonClick(ClickEvent event) {}
}
