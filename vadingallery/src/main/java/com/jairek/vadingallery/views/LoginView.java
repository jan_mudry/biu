package com.jairek.vadingallery.views;

import org.vaadin.appfoundation.view.AbstractView;
import org.vaadin.appfoundation.view.ViewHandler;
import com.jairek.vadingallery.MyVaadinApplication;
import com.jairek.vadingallery.domain.Person;
import com.jairek.vadingallery.service.AuthService;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Window.Notification;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

public class LoginView extends AbstractView<VerticalLayout> implements ClickListener {
	  private static final long serialVersionUID = 8401760557369059696L;

	  private Button btnLogin = new Button("Login");
	  private TextField login = new TextField ( "Username");
	  private TextField password = new TextField ( "Password");
	  private AuthService authService = new AuthService();
	  
	  public LoginView() {
	    super(new VerticalLayout());
	    
	    password.setSecret ( true );
	    getContent().addComponent ( new Label ("Please login in order to use the application") );
	    getContent().addComponent ( new Label () );
	    getContent().addComponent ( login );
	    getContent().addComponent ( password );
	    getContent().addComponent ( btnLogin );

	    btnLogin.addListener ( new ClickListener() {
			private static final long serialVersionUID = 1L;

			public void buttonClick ( Button.ClickEvent event )
            {
                try
                {
                	Person user = authService.login((String)login.getValue (), (String)password.getValue ());
                	if (user != null) {
                    	MyVaadinApplication.getCurrent().setUser(user);
                    	ViewHandler.activateView(WelcomeView.class);                 	
                	}
                }
                catch ( Exception e )
                {
                	MyVaadinApplication.getCurrent().getMainWindow().showNotification ( "Error" + e.getMessage(), Notification.TYPE_ERROR_MESSAGE );
                }
            }
        });		
	  }

	public void activated(Object... params) {}
	public void deactivated(Object... params) {}
	public void buttonClick(ClickEvent event) {}
}