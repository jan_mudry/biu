package com.jairek.vadingallery.views;

import java.io.File;

import org.vaadin.appfoundation.view.AbstractView;

import com.jairek.vadingallery.MyVaadinApplication;
import com.jairek.vadingallery.domain.Photo;
import com.jairek.vadingallery.domain.WhichPhoto;
import com.jairek.vadingallery.service.EntityManager;
import com.vaadin.terminal.FileResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

public class ShowAlbumView extends AbstractView<VerticalLayout>  {
	private static final long serialVersionUID = 1L;
	private int album_id; 
	Button bPrev, bNext, bFirst, bLast, bRandom = null;
	private int actualPhotoId;
	private int lastPhotoId;
	private int firstPhotoId;
	Embedded  image;
	Label descriptionImage;
	EntityManager em = new EntityManager();
	
	public ShowAlbumView() {
		super(new VerticalLayout());
		image = new Embedded();
		image.setType(Embedded.TYPE_IMAGE);
		image.setStyleName("album-photo");
		image.setHeight("300px");
		descriptionImage = new Label();
		descriptionImage.setStyleName("description-image");
		descriptionImage.setHeight("30px");
	}

	public void activated(Object... params) {
		album_id = (Integer) params[0];
		actualPhotoId = 0;
		getContent().setSpacing(true);
		addComponentInCenter(image);
		addComponentInCenter(descriptionImage);
		getContent().addStyleName("album-image-container");
		HorizontalLayout navButtons = getNavPhotoButtons(album_id);
		addComponentInCenter(navButtons);

		Photo firstPhoto = em.getPhoto(album_id, MyVaadinApplication.getCurrent().getUser().getId(), WhichPhoto.FIRST, 0);
		Photo lastPhoto = em.getPhoto(album_id, MyVaadinApplication.getCurrent().getUser().getId(), WhichPhoto.LAST, 0);
		firstPhotoId = firstPhoto.getId();
		lastPhotoId = lastPhoto.getId();
		System.out.println("firstPhotoId" + firstPhotoId);
		
		setPhoto(firstPhoto);
	}

	public void deactivated(Object... params) {
		getContent().removeAllComponents();
	}
	
	public void addComponentInCenter(Component component) {
		getContent().addComponent(component);
		getContent().setComponentAlignment(component, Alignment.MIDDLE_CENTER);
	}
	
	public void buttonClick(ClickEvent event) {}
	
	@SuppressWarnings("serial")
	class ClickNavigationButtonHandler implements Button.ClickListener {
		int albumId;
		WhichPhoto which;
		
		public ClickNavigationButtonHandler(){}
		public ClickNavigationButtonHandler(WhichPhoto which2, int album_id){
			albumId = album_id;
			which = which2;
			
		}
		public void buttonClick(ClickEvent event) {
			Photo result = em.getPhoto(albumId, MyVaadinApplication.getCurrent().getUser().getId(), which, actualPhotoId);
			setPhoto(result);
		}		
	} 	
	

	
	HorizontalLayout getNavPhotoButtons(int album_id) {
		HorizontalLayout hpanel = new HorizontalLayout();
		hpanel.addStyleName("nav-buttons-container");
				
		bPrev = new Button("<");
		bPrev.addListener(new ClickNavigationButtonHandler(WhichPhoto.PREV, album_id));
		bNext = new Button(">");	
		bNext.addListener(new ClickNavigationButtonHandler(WhichPhoto.NEXT, album_id));
		bFirst = new Button("<||");	
		bFirst.addListener(new ClickNavigationButtonHandler(WhichPhoto.FIRST, album_id));
		bLast = new Button("||>");	
		bLast.addListener(new ClickNavigationButtonHandler(WhichPhoto.LAST, album_id));
		bRandom = new Button("RANDOM");	
		bRandom.addListener(new ClickNavigationButtonHandler(WhichPhoto.RANDOM, album_id));
		hpanel.addComponent(bFirst);
		hpanel.addComponent(bPrev);
		hpanel.addComponent(bRandom);
		hpanel.addComponent(bNext);
		hpanel.addComponent(bLast);
		
		return hpanel;
	}
	
	
	void setPhoto(Photo photo) {
		if (photo != null) {
			image.setSource(new FileResource(new File(photo.getPath()), MyVaadinApplication.getCurrent()));
			actualPhotoId = photo.getId();
			descriptionImage.setCaption(photo.getDescription());
			if (photo.getId() == firstPhotoId) {
				bFirst.setEnabled(false);
				bPrev.setEnabled(false);
			} else {
				bFirst.setEnabled(true);
				bPrev.setEnabled(true);
			}
			if (photo.getId() == lastPhotoId) {
				bLast.setEnabled(false);
				bNext.setEnabled(false);
			} else {
				bLast.setEnabled(true);
				bNext.setEnabled(true);
			}
			if  (photo.getId() != lastPhotoId && photo.getId() != firstPhotoId) {
				bFirst.setEnabled(true);
				bLast.setEnabled(true);
				bNext.setEnabled(true);
				bPrev.setEnabled(true);
			}
		}
	}
}