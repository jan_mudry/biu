package com.jairek.vadingallery.domain;

public enum WhichPhoto {
	FIRST, LAST, NEXT, PREV, RANDOM
}
